# my coding hut blog (walter76.gitlab.io)

This is the version 2 of my blog "my coding hut". It is build using the static
site generator [zola](https://www.getzola.org/) and makes use of the
[after-dark](https://github.com/getzola/after-dark) theme.

It is deployed on gitlab pages as [my coding hut](https://walter76.gitlab.io).
