# Articles Not Finished

- `about-usb`
- `creating-a-corona-restaurant-tracking-app-part3`
- `on-object-orientation-in-rust.md`

# Rework

- Change wording of guest tracking part 1 to "we" and "you"

# Maintenance

- update after-dark theme to newest version (`HEAD DETACHED`?)

