+++
draft = true

title = "About USB"
date = 2023-03-12

[taxonomies]
tags = ["rust", "usb"]
+++

I own a Rival 3 wireless mouse from SteelSeries and I love it. I also recently
switched to Manjaor Linux being my main operating system. Sadly SteelSeries
does not offer a nice UI and I really wanted to monitor the battery of my
mouse. So I thought, this can't be too hard to get this information. And so,
my friends, I opened the box of pandora, in this case I started learning USB.

<!-- more -->
