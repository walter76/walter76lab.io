+++
title = "Create a Firebase hosted React App"
date = 2020-07-20

[taxonomies]
tags = ["react", "javascript", "firebase"]
+++

So, I was curious how to host an app created with [React](https://reactjs.org/)
on Google's [Firebase](https://firebase.google.com/).

Actually it turned out to be pretty simple and the article just outlines what
needs to be done. Let's get started and create a simple React app, so we have
something to toy with.
<!-- more -->
# Create a React app with `create-react-app`

To scaffold the new project, I am using [create-react-app](https://github.com/facebook/create-react-app)
from facebook. Open a terminal, navigate to the folder where you usually have
your source code and type: 

```sh
npx create-react-app hello-firebase-hosting
```

Your React app will be created in a new folder `hello-firebase-hosting`. The 
process will take a while as `create-react-app` is downloading all dependencies
and setting everything up. While it is working your screen will look similar to
what you see below.

![](01_create-react-app.png)

When the project has been set up successfully you will see a console message
with `Happy hacking!`. Now we are good to go.

We do not do anything else with this project as our goal is to just have
something that can be deployed to Firebase. If you want to check that it is
running, you can type `yarn start` at the console. This will run the project,
locally serve the app and automatically open your browser showing it. 

# Install the Firebase CLI

To be able to prepare your project for Firebase deployment and actually push it
to the cloud, you need to install the Firebase CLI with:

```sh
npm install -g firebase-tools
```

This will globally install the Firebase CLI. Afterwards you have a new command
available in your shell: `firebase`. You can check if it is working with:

```sh
firebase --version
```

This should print the version of Firebase CLI to the console. During writing of
this article, I have `8.0.2` installed.

# Setup the project on Firebase

Next you need to setup the project on Firebase. Just open a browser and navigate
to https://firebase.google.com/. If you have a Google account, you can use that
to sign in, otherwise you need to sign up for an account.

You can get started immediately without the need to pay for anything as there is
a free plan associated with Firebase. Up to now, I have not hit any limits
preventing me to toy around without paying.

I am roughly describing the steps needed to set everything up. If you want to dig
deeper into the details, I recommend the Firebase documentation. You can start at
[Add Firebase to your JavaScript project](https://firebase.google.com/docs/web/setup?authuser=0).

## Step 1: Create a Firebase project

After successful login you are presented with the following screen.

![](02_firebase-landing-after-login.png)

Click on **Go to console** in the upper right corner to continue.

You see a screen with all your projects. Next click on **Add project** to create
a new one. Then select or enter a **Project name**. Click **Continue.**

You can now optionally set up [Google Analytics](https://analytics.google.com/analytics/web/)
which is helpful if you want to monitor the usage of your React app. For our toy
project, we do not need it, so just disable it and click on **Create project**.

Firebase now automatically provisions resources for your project. When the
process completes, you'll be taken to the overview page for your newly created
project in the Firebase console.

## Step 2: Register your app with Firebase

A Firebase project provides a lot of different resources for creating
applications hosted in the cloud. Each of those need to be enabled and
configured individually. If you plan to create a web app, like we do, you need
to add it to your project.

In the center of the Firebase console's project overview page, click the **Web**
icon (**</>**) to launch the setup workflow. Enter a nickname, e.g.
`hello-firebase-app`, for your app.

You do not have to setup Firebase hosting now, we will do this later with the
CLI. Next, just click **Register app**. That's it for now in the console.

# Prepare the project for hosting

The next step is to prepare the project for hosting. Let's switch back to our
local terminal and login to Firebase with the CLI:

```sh
firebase login
```

It will ask for your credentials. After you have logged in, you need to **switch
to the root directory** of your React app. In our case this will be
`hello-firebase-hosting`.

Now let's initialize the project for Firebase:

```sh
firebase init
```
It will take a while but then you will presented with a welcome screen. It will
look similar to the following screenshot.

![](03_firebase-init-step-1.png)

If you see a message, like `* You are currently outside your home directory`,
you can ignore it. Hit `Y` to continue. Now you need to choose what you want to
do. Use your arrow keys and select **Hosting** with the *space*-key. Hit *enter*
to continue. Next, select **Use an existing project**.

You now get a list of projects that you have created on Firebase. Select the one,
we have just created and hit *enter*. I select `walters-blog` just to have one
for this example.

We are done with the project setup and are now setting up hosting. The Firebase
CLI needs to know the directory where it should take the content to publish to
the cloud. The default is `public`, but if we build our React app for production,
`create-react-app` will put the release into the folder `build`. So, we need to
change that. Type `build` when asked `? What do you want to use as your public
directory?  (public)` and hit *enter*.

The last question will be if you want to configure it as a single-page app. For
our simple example you can go with the default setting here. We are done! The
output on the terminal should look similar to the following screenshot.

![](04_firebase-init-done.png)

# Build your project

Time to get ready for production and build our project. I am using
[yarn](https://yarnpkg.com/) for package management, so I will do:

```sh
yarn build
```

This will build your application and put the results into the folder `build`.

# Deployment

Our final step is, to put it into production and deploy the application. This
is a rather small step now. Just enter

```sh
firebase deploy
```

in the console and it should deploy to Firebase. It will print the URL to the
console after deployment is finished. Open up your browser and try it out!

# Git Users

If you are using [git](https://git-scm.com/), please be sure to add the folder
`.firebase/*` to your `.gitignore` file. The folder is used by the Firebase CLI
as a local cache folder to determine what has been already put to hosting and
what needs to be updated. This folder should not end up in your repository.

# Summary

In this article, we have created a bare minimum React app to put it into the
web. For that, we have set up a Firebase project and initialized hosting.
Afterwards we have build our application and put it into production.
