+++
title = "Creating a Corona restaurant guest registration app - Part 1"
date = 2022-03-02

[taxonomies]
tags = ["react", "javascript", "styled-components"]
+++

This is an article about how one could create a very simple guest registration
app. Back in 2020 I was disappointed by all the manual pen and paper guest
registrations that I decided to explore how a digital solution could look like.
Today there are plenty of apps available, e.g. [luca](https://www.luca-app.de/),
so this post and the series is just an example of how to develop a web
application.
<!-- more -->

# Introduction

I don't know about your country, but since Germany got out of the full contact
restrictions there are some rules to be followed. They have been put into place
to control the pandemic and prevent another lockdown. One of those rules is, that
whenever you go to a restaurant, you have to leave your name and phone number.
In case somebody is tested positive for Covid-19 later on, the authorities can
track possible contacts and do localized testing to contain the further spread
of the pandemic.

Since this rule has been put into place, I have been wondering how far behind
Germany is in terms of digitalization. In the majority of restaurants, you
will get a sheet of paper and a pen where you write down your contact
information. There are exceptions where this is done electronically with a
mobile app, but my experience is, that this is a rare case. *(I have written this
back in 2020, today things have changed. I think in the majority of restaurants
you actually now have a digital solution.)*

So, this pen and paper (no, we are not doing a roleplaying game ;-)) has several
downsides:

* The pen and paper is touched by you and others and therefore it can support
  spreading the pandemic. Some might clean it, but to be honest I have rarely
  seen this.
* It is paper and the restaurant needs to archive it somewhere.
* It is paper and likely to get lost.
* When properly handled to track down infections later on, somebody should
  digitalize it. This means typing the information into some application (most
  likely Excel :-)).
* The process is error prone and makes it hard to track down people in case of
  an infection.
* It is also hard to control if somebody leaves his real contact information.

Having an app where you just type in the URI in your smart phone (or scan an QR
code) to enter your contact details is much simpler and makes it easier tracking
down people later on. It is also less error prone. For sure you need to deal
with the data privacy issues, but I think this can be managed.

If you plan to follow along, you can find the code on [gitlab](https://gitlab.com)
as [guest-tracker](https://gitlab.com/walter76/guest-tracker).

# Requirements

As always I am going to do this in an incremental manner. The goal will be to
have a minimum viable product in the end. The simplest useful thing to have
would be:

* A registration form for guests to enter there personal data and table number
* A tabular report at which day and time guests were at the restaurant

To make the initial product less complex, I will hard-code some things that
should be configurable in a real-world multi-tenancy application. But I will try
to keep a usage by different customers in mind and prepare a little bit already.
This should also make it easier for somebody else to adapt the code to their
needs.

I have quickly sketched out some UI with my favorite drawing tool [draw.io](https://draw.io).

![](01_mockup-registration-form.png)

The picture above shows the registration form. The next one shows a rough sketch
of the result table.

![](02_mockup-result-table.png)

Most likely the UI will change during the process of development and while I add
new features.

# Setting up the project

Starting a new React project is always the same process. I will fire up a
console and enter:

```sh
npx create-react-app guest-tracker-2
```

*(The project is called `guest-tracker-2` because I am reengineering the app
with a different tech stack.)*

This command is going to scaffold a new React project in the folder
`guest-tracker-2`. It looks like this after it has finished.

![](03_create-react-app.png)

Now I need to do some clean-up work for the project. Opening up Visual Studio
Code, the folder currently looks like this:

![](04_vscode-initial-project.png)

I will use [styled-components](https://styled-components.com/) for the UI, so I
do not need CSS. I also don't want tests right now. Let's get rid of the following
files: `App.css`, `App.test.js`, `index.css`, `logo.svg` and `setupTests.js`.
Afterwards the directory looks like this:

![](05_vscode-remove-files.png)

Next I remove everything from `App.js` and replace it with the following code.
This step is just to get a clean start.

```js
import React from 'react'

const App = () => <div>guest-tracker-app</div>

export default App
```

And finally, I remove the import of `index.css` from `index.js` and do some
formatting.

```js
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
```

Next, I switch to the folder `public` and `index.html`. I need to change the
meta tag for the description and the title.

```html
<!-- snip -->
<meta
  name="description"
  content="simple app to track down guests attending an event"
/>
<!-- snap -->

<!-- snip -->
<title>guest-tracker</title>
<!-- snap -->
```

I would also like to use the [Roboto font](https://fonts.google.com/specimen/Roboto)
from Google. We need to reference it in `public/index.html`. So I add the
following line above the `<title>` tag:

```html
<!-- snip -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
<!-- snap -->
```

And finally I have to adapt `manifest.json`.

```json
// snip
"short_name": "guest-tracker",
"name": "guest-tracker",
// snap
```

Now it is time to fire up the app for the first time and check if I made any
mistakes during cleaning up. After entering `yarn start` in the console I get
the following screen in the browser window.

![](06_start-after-cleanup.png)

Looks like everything went well. Moving on.

# Styled Components

In the previous version of this series, I had used [Google Material UI](https://material.io/)
for the color selection and [material-ui](https://material-ui.com/) for the UI
components. But that made things too complicated and lengthy. I will now go with
[styled-components](https://styled-components.com/). A much easier approach from
my perspective.

So, let's first add `styled-components` to our project with:

```
yarn add styled-components
```

We want to have some styles that are globally applied to the complete web
application. To do so, we create a file `globalStyle.js` in the `src` folder.

```js
import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    font-family: -apple-system, BlinkMaxSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica New",
      sans-serif;
  }
`

export default GlobalStyle
```

We reset the `margin` and `padding` for the `body` and set our global font
style.

This will not work yet. We have to apply it in `App.js`.

```js
import React from 'react'

import GlobalStyle from './globalStyle'

const App = () => (
  <>
    <GlobalStyle />
    <div>guest-tracker-app</div>
  </>
)

export default App
```

Let's add our first real style and create a UI wrapper for the application. We
have to import `styled` from `styled-components`:

```js
// snip
import styled from 'styled-components'
// snap
```

A styled component basically is a React component that has some styles applied.
To create one, you need to use `styled` and base it on a html tag. Our wrapper
will be a `div`. Right after the `import` statements add:

```js
// snip
const AppWrapper = styled.div`
  background-color: #fafafa;
  margin: 5px auto;
  padding: 2px 10px 2px 10px;
  max-width: 75%;
`
// snap
```

This creates a styled component `AppWrapper` with the defined CSS styles. I also
want to have a style for the application title. It will just be an empty style
for now:

```js
// snip
const AppTitle = styled.h1``
// snap
```

Now we need to use it in our component `App`:

```js
// snip
<>
  <GlobalStyle />
  <AppWrapper>
    <AppTitle>guest-tracker-app</AppTitle>
  </AppWrapper>
</>
// snap
```

The complete code for `App.js` should now look like this:

```js
import React from 'react'
import styled from 'styled-components'

import GlobalStyle from './globalStyle'

const AppWrapper = styled.div`
  background-color: #fafafa;
  margin: 5px auto;
  padding: 2px 10px 2px 10px;
  max-width: 75%;
`

const AppTitle = styled.h1``

const App = () => (
  <>
    <GlobalStyle />
    <AppWrapper>
      <AppTitle>guest-tracker-app</AppTitle>
    </AppWrapper>
  </>
)

export default App
```

Let's run it once to see if everything is working fine.

![](07_final_app_part_1.png)

# Summary

That's it for the first part of the series. Today, I have described my idea and
why I am going to build a Corona restaurant tracking app. After defining some
requirements for an MVP I have setup the project and installed the basics for
the UI.

In the next post, I will build the form for registration of an guest.

If you want to follow along, check out the repo at [gitlab](https://gitlab.com/walter76/guest-tracker).
Be aware that I am currently reengineering the series. Therefore you will find
two folders in `src`. The folder `guest-tracker-app` contains the previous
version.
