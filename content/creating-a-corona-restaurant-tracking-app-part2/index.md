+++
title = "Creating a Corona restaurant guest registration app - Part 2"
date = 2022-03-22

[taxonomies]
tags = ["react", "javascript", "styled-components"]
+++

In [part 1](@/creating-a-corona-restaurant-tracking-app-part1/index.md)
we have explored the idea and requirements for the Corona restaurant guest
tracking app. Then we setup the project with [React](https://reactjs.org/) and
made some preparations for the styling of the UI with [styled-components](https://styled-components.com/).

As I have already described in the previous parts, the project is entirely open
source. You can check it out on [gitlab](https://gitlab.com/walter76/guest-tracker).

In this part, we will build out the form for registering as a guest. We will not
yet introduce some backend. This we save for later.
<!-- more -->

# Restructuring the Code

Before digging into the implementation of the guest registration form, we need
to do a little bit of refactoring to have a better project structure. I prefer
to [Structure files as feature folders or ducks](https://redux.js.org/style-guide/style-guide#structure-files-as-feature-folders-or-ducks)
which is described in the [Redux sytle guide](https://redux.js.org/style-guide/style-guide). 

First, we create a new folder `app` right below `src` and move the files `App.js`
, `globalStyle.js` into `app`. Afterwards, we update the import in `index.js` to
point to `src/app/App.js`.

```js
import App from './app/App'
```

# Adding the Guest Registration Form Feature

We will organize the code in a feature-wise manner, i.e. there is a folder
called `feature` which includes a subfolder for each major functionality.

Create a folder `src/features/registration` and inside create a new file
`Registration.js` which will be the main component for the guest registration
form. The code looks like that:

```js
import React from 'react'

const Registration = () => <div>Guest Registration</div>

export default Registration
```

Hook the `Registration` up in `App.js`:

```js
// snip
import Registration from '../features/registration/Registration'
// snap

// snip
    <AppWrapper>
      <AppTitle>guest-tracker-app</AppTitle>
      <Registration />
    </AppWrapper>
// snap
```

With that we have created the skeleton for the guest registration form. Starting
the app with `yarn start` will show the following screen which is as
good as it gets for now.

![](01_registration-form-init.png)

# Adding Fields

Let's quickly recap how we wanted the guest registration form to look like. The
initial drawing looked like that.

![](02_mockup-registration-form.png)

We start with the page layout and a header. In `Registration.js` change the code
as follows.

```js
import React from 'react'
import styled from 'styled-components'

const Page = styled.div``

const Title = styled.h2``

const Registration = () => (
  <Page>
    <Title>Welcome to A Great Place to Eat!</Title>
  </Page>
)

export default Registration
```

After the required import we add two styled components `Page` and `Title` which
for starters just refer to some HTML tags. Those components are then used to set
up the layout and show a page title.

As stated in the first blog post, we want to introduce some variability to make
this work for different restaurants right from the beginning. Therefore, we will
make the name of the restaurant configurable. Introduce a new file
`Configuration.js` in a new folder `src/common`.

```js
const Configuration = {
  name: 'AGreatPlaceToEat',
}

export default Configuration
```

Then, you can use it in `Registration.js`:

```js
// snip
import Configuration from '../../common/configuration'
// snap

// snip
const Registration = () => (
  <Page>
    <Title>Welcome to {Configuration.name}!</Title>
  </Page>
)
// snap
```

So far our guest registration form is looking like that.

![](03_registration-form-header.png)

Let's now add all the fields and the buttons for `Register` and `Cancel`. The
UI elements will not do anything besides being displayed.

Start by adding two more styles, `FormFieldsContainer` that will layout all the
fields in the form, and `ButtonRow` which will be used to align the buttons.

```js
// snip
const FormFieldsContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 5px;
`

const ButtonRow = styled.div`
  display: flex;
  justify-content: space-between;
`
// snap
```

Both styled components are `div` and they use the CSS3 Flex Layout (see
[A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
as a great reference) to layout elements with that HTML feature.

Next we add the fields and buttons.

```js
// snip
<Page>
  <Title>Welcome to {Configuration.name}!</Title>
  <FormFieldsContainer>
    <input type='text' placeholder='Lastname' autoFocus />
    <input type='text' placeholder='Firstname' />
    <input type='text' placeholder='Mobile' />
    <hr />
    <input type='text' placeholder='Table number' />
    <input type='text' placeholder='Date' />
    <input type='text' placeholder='From' />
    <input type='text' placeholder='To' />
    <hr />
    <ButtonRow>
      <input type='submit' value='Register' />
      <input type='button' value='Cancel' />
    </ButtonRow>
  </FormFieldsContainer>
</Page>
// snap
```

The resulting page will now look like that.

![](04_registration-form-fields-and-buttons.png)

# Adding State and Initial Behavior

Besides displaying all the fields and buttons, this is not doing much yet. As we
do receive input from the user, the guest registration form  needs to have some
state. We start with the first small step and add this just for `lastname`.

```js
// snip
import React, { useState } from 'react'
// snap

// snip
const Registration = () => {
  const [lastname, setLastname] = useState('')

  const onSubmit = (event) => {
    console.log({
      lastname,
    })

    event.preventDefault()
  }

  return (
    <Page>
      <Title>Welcome to {Configuration.name}!</Title>
      <form onSubmit={onSubmit}>
        <FormFieldsContainer>
          <input
            type='text'
            placeholder='Lastname'
            value={lastname}
            onChange={(e) => setLastname(e.target.value)}
            autoFocus
          />
          <input type='text' placeholder='Firstname' />
          <input type='text' placeholder='Mobile' />
          <hr />
          <input type='text' placeholder='Table number' />
          <input type='text' placeholder='Date' />
          <input type='text' placeholder='From' />
          <input type='text' placeholder='To' />
          <hr />
          <ButtonRow>
            <input type='submit' value='Register' />
            <input type='button' value='Cancel' />
          </ButtonRow>
        </FormFieldsContainer>
      </form>
    </Page>
  )
}
// snap
```

We have to change the signature of the function to `const Registration = () => {`
as we need some functionality now and not simply returning some *static* JSX
anymore. Inside we added:

```js
const [lastname, setLastname] = useState('')
```

This creates a local state for `lastname` and changes the React component to be
stateful (see [State and Lifecycle](https://reactjs.org/docs/state-and-lifecycle.html)
for more details).

If you are not used to it, the syntax looks a bit strange. The method `useState`
is a React [hook](https://reactjs.org/docs/hooks-intro.html). It is returning an
array with a variable reflecting the current state and a function to update it.
We make use of that here:

```js
<input
  type='text'
  placeholder='Lastname'
  value={lastname}
  onChange={(e) => setLastname(e.target.value)}
  autoFocus
/>
```

Notice `value={lastname}` this is accessing the current state and showing it in
the text input field. In addition `onChange={(e) => setLastname(e.target.value)}`
calls `setLastname` to update the value.

To be able to really send the data, we also wrap the fields into a
`<form></form>` tag. The handler `onSubmit` will be called if the user clicks on
the button `Register`. The reason for that is, that the `type` of the `input` is
declared as `submit`, and is inside the `<form></form>` tag.

When we now start the app, we can enter some text into the field `lastname` and
click `Register`. This calls the `console.log` that we put into the handler
`onSubmit`. If you open the Web Developer Tools in your favorite browser, you
can see the output in the javascript console.

![](05_registration-form-adding-state.png)

All the other fields will follow the same logic. Again, we start with the
declaration of the state.

```js
// snip
const [lastname, setLastname] = useState('')
const [firstname, setFirstname] = useState('')
const [mobile, setMobile] = useState('')
const [tableNumber, setTableNumber] = useState('')
const [date, setDate] = useState('')
const [from, setFrom] = useState('')
const [to, setTo] = useState('')
// snap
```

Afterwards we hook it up to all the input fields.

```js
// snip
<input
  type='text'
  placeholder='Lastname'
  value={lastname}
  onChange={(e) => setLastname(e.target.value)}
  autoFocus
/>
<input
  type='text'
  placeholder='Firstname'
  value={firstname}
  onChange={(e) => setFirstname(e.target.value)}
/>
<input
  type='text'
  placeholder='Mobile'
  value={mobile}
  onChange={(e) => setMobile(e.target.value)}
/>
<hr />
<input
  type='text'
  placeholder='Table number'
  value={tableNumber}
  onChange={(e) => setTableNumber(e.target.value)}
/>
<input
  type='text'
  placeholder='Date'
  value={date}
  onChange={(e) => setDate(e.target.value)}
/>
<input
  type='text'
  placeholder='From'
  value={from}
  onChange={(e) => setFrom(e.target.value)}
/>
<input
  type='text'
  placeholder='To'
  value={to}
  onChange={(e) => setTo(e.target.value)}
/>
// snap
```

And finally we add it to the `onSubmit` handler.

```js
// snip
const onSubmit = (event) => {
  console.log({
    lastname,
    firstname,
    mobile,
    tableNumber,
    date,
    from,
    to,
  })

  event.preventDefault()
}
// snap
```

# Implementing Behavior for the Cancel Button

Let's add some very basic functionality for the cancel button, too. First, we
add a new handler method which will just clear the state for all input fields.

```js
// snip
const onCancel = (_) => {
  setLastname('')
  setFirstname('')
  setMobile('')
  setTableNumber('')
  setDate('')
  setFrom('')
  setTo('')
}
// snap
```

Then, we hook the `onCancel` handler with the `onClick` event of the cancel 
button.

```js
// snip
<ButtonRow>
  <input type='submit' value='Register' />
  <input type='button' value='Cancel' onClick={onCancel} />
</ButtonRow>
// snap
```

Now we have a nice registration from with the ability to clear all the fields on
hitting cancel.

![](06_registration-form-add-state-to-all-fields.png)

# Summary

We are done with part 2. Today, we have created a first iteration of the actual
registration form and added some basic functionality. On submitting, we just log
the content of the input fields to the console, while on cancel we clean all the
fields.

There is still much to do, e.g. the layout could be much more pretty, we could
add validation of input fields, and so on. We save this for later blog posts.

We also have not created a backend service yet and connected the app. This is,
what we will do in part 3.

If you want to follow along, check out the repo at [gitlab](https://gitlab.com/walter76/guest-tracker).
Be aware that I am currently reengineering the series. Therefore you will find
two folders in `src`. The folder `guest-tracker-app` contains the previous
version, while `guest-tracker-2` contains the reworked one.
