+++
draft = true

title = "Creating a Corona restaurant guest registration app - Part 3"
date = 2022-03-30

[taxonomies]
tags = ["react", "javascript", "firebase"]
+++

In part 3 of the Guest Tracker application we will work on the backend. To keep
it simple, we will create a minimal web API with the dotnet CLI and adapt it to
our needs. At the end of the post we will test it with some simple `curl`
commands.

<!-- more -->
If you have discovered this post just now, and am curious on the other parts, you
can find them here:

* [Part 1 - Creating a Corona restaurant guest registration app](@/creating-a-corona-restaurant-tracking-app-part1/index.md)
* [Part 2 - Guest registration form](@/creating-a-corona-restaurant-tracking-app-part2/index.md)

As I have already described in the previous parts, the project is entirely open
source. If you want to follow along, check out the repo at [gitlab](https://gitlab.com/walter76/guest-tracker).
Be aware that there are two folders in `src`. The folder `guest-tracker-app`
contains the previous version of the frontend, while `guest-tracker-2` contains
the reworked one. The backend is supposed to be connected to the latter.

# REST API

Before we get started it is a good idea to think a little bit on the REST API
that we would need.

| URI | Method | Description | Request body | Response body |
| --- | ------ | ----------- | ------------ | ------------- |
| `/api/registrations` | `POST` | Call for registering a guest | `Guest` schema | *None* |

Up to now we just support one REST API used to register a guest. As we create a
new resource, we use `POST` as a HTTP method. The API requires the information
on the registration as JSON. The `Guest` schema looks like that:

```
Guest {
    lastname string
    firstname string
    phone string
    tableNumber integer($int32)
    date string($date)
    from string($time)
    to string($time)
}
```

# Scaffolding the Project

We will use .NET 6 for the backend. If you want to follow along and have not
installed it on your system, you need to do it. Just download the SDK from
[Download .NET 6.0](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
and follow the installation instructions.

After the successful installation, change to our source folder `src` and type:

```sh
dotnet new webapi -minimal -o GuestTrackerServer
```

This will scaffold a new minimal web API project in the folder
`GuestTrackerServer`. Afterwards change to the created directory and start your
IDE. With Visual Studio Code you would type:

```sh
code -r ../GuestTrackerServer
```

Below you will find a screenshot of my shell for doing just that.

![](01_create_minimal_webapi_project.png)

If you now look at `Program.cs` in your IDE it will look like that:

```cs
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/weatherforecast", () =>
{
    var forecast =  Enumerable.Range(1, 5).Select(index =>
        new WeatherForecast
        (
            DateTime.Now.AddDays(index),
            Random.Shared.Next(-20, 55),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();
    return forecast;
})
.WithName("GetWeatherForecast");

app.Run();

record WeatherForecast(DateTime Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}
```

This is the example web API that has been created by the dotnet CLI. We will
explain and adapt it to our needs.

The first line is just creating a builder for the web application. But it is
followed by some interesting part:

```cs
// snip

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// snap
```

To figure out what this is doing, let us start the application with the following
command:

```sh
dotnet run --launch-profile GuestTrackerServer
```

This will launch the web backend with a environment specified as a launch
profile. For now, just think about that as a specific configuration for starting
your application in development mode.

If you want to learn more about launch profiles, you can look it up in the
chapter [Development and launchSettings.json](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-6.0#lsj) of the ASP.NET Core 6.0 documentation.

Let's have a look at `Properties/launchSettings.json` to figure out what the
launch profile `GuestTrackerServer` is doing.

```json
{
  "$schema": "https://json.schemastore.org/launchsettings.json",
  "iisSettings": {
    "windowsAuthentication": false,
    "anonymousAuthentication": true,
    "iisExpress": {
      "applicationUrl": "http://localhost:5066",
      "sslPort": 44344
    }
  },
  "profiles": {
    "GuestTrackerServer": {
      "commandName": "Project",
      "dotnetRunMessages": true,
      "launchBrowser": true,
      "launchUrl": "swagger",
      "applicationUrl": "https://localhost:7211;http://localhost:5002",
      "environmentVariables": {
        "ASPNETCORE_ENVIRONMENT": "Development"
      }
    },
    "IIS Express": {
      "commandName": "IISExpress",
      "launchBrowser": true,
      "launchUrl": "swagger",
      "environmentVariables": {
        "ASPNETCORE_ENVIRONMENT": "Development"
      }
    }
  }
}
```

Under `profiles` you will find `GuestTrackerServer` which is the profile that we
just used for starting the web backend. Inside you will find this:

```json
      "environmentVariables": {
        "ASPNETCORE_ENVIRONMENT": "Development"
      }
```

This is starting the .NET application and setting the environment variable
`ASPNETCORE_ENVIRONMENT` to `Development`. If you look back at the code from the
initial `Program.cs` you find:

```cs
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
```

So, setting the environment variable will start the application in development
mode and the `if`-statement calls two methods in this case: `app.UseSwagger()`
and `app.UseSwaggerUI()`.

BTW you could achieve the same by just setting the environment variable in your
shell before doing `dotnet run`. Like so:

```shell
ASPNETCORE_ENVIRONMENT=Development dotnet run
```

This is how that works in Linux and MAC OS. Windows works differently. Microsoft
has some nice documentation which explains everything [Set the environment by setting an environment variable](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-6.0#set-the-environment-by-setting-an-environment-variable).

Let's now get back to our source code and figure out what the snippet above is
really doing.

If your browser did not open automatically, navigate to the `applicationUrl`
provided in the profile and append `/swagger` to it. In our case that is
https://localhost:7211/swagger.

![](02_swagger_ui.png)

This is nice, we see a browsable documentation of our REST API and also have the
possibility to test it. When configured with the code that we just looked at,
ASP.NET Core 6.0 supports [OpenAPI](https://www.openapis.org/) and [Swagger](https://swagger.io/)
out-of-the-box.

So, no need for cryptic `curl` from the command line to test our REST API. We
just use the swagger UI.

The next line in the `Program.cs` is:

```cs
// snip
app.UseHttpsRedirection();
// snap
```

This activates redirection of http requests to use https. As the majority of
browsers nowadays will warn when you use http and not https, let's keep this
line.

This is all regarding the bootstrapping of the REST API. Everything else, except
`app.Run()` in the file `Program.cs`is the logic. Let's delete all the remaining
lines, except `app.Run()`. My `Program.cs` now looks like this:

```cs
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

// REST API goes here

app.Run();
```

# Coding the REST API

