+++
title = "Focused work: Use filters when you feel overwhelmed - Part 1"
date = 2020-05-26

[taxonomies]
tags = ["focus", "productivity"]
+++

This will be the first article in a series of non-technical articles about my
personal way of working.

Due to Covid-19 crisis many people are working from home these days, and so am
I. Besides a lot of positive things, I have also experienced a need to be even
more structured and organized in both, my professional and private life. This
series of articles is a wrap-up of all kind of tactics that I apply to structure
my work and organize my days.
<!-- more -->
The system of tactics I have put together is not perfect and maybe it never will
be, but I am constantly learning from my mistakes and adapting. While you are
reading the articles in the series, please keep in mind that this all is part of
my personal system. Therefore some of the techniques working for me, might not
yield the same results for you.

I have read endless blogs and books about different work organization topics and
tried to apply them. I have had my fair share of [GTD - Getting Things Done](https://gettingthingsdone.com/)
by David Allen, Eat that Frog by Brian Tracy, blog posts on [Medium](https://medium.com/)
and so own. Nothing really worked for me as it was explained in the books and
articles. There have been countless failed experiments of adapting the
strategies in my private and professional life. Finally I realized, that what
authors write will never work the same way for you as it does for them. Still,
constantly learning and adapting from the different approaches will eventually
lead to your own "imperfect" system. Therefore, my sincere hope is that you will
find some of my tactics interesting enough to give them a try. The best case
will be if you discover something that helps you to improve too.

# Energy and filters

Through my day, I just have a certain amount of energy to spend for work, family
and leisure. There are things that eat up less energy, e.g. sorting my e-mails,
and things that require more focus (energy), e.g. working on a new architectural
concept or spending quality time with my wife and kids. In addition, if I go
exercising (running for me is perfect) it kind of restores some of my energy.
But during the day my energy level will never get back to the state that I had
in the morning. For this I would require a good night of sleep. During the week
I usually can cope with 6 hours, but 7.5 hours is perfect.

If you want to learn more about sleep cycles and how much you would need, I can
recommend [How to calculate when you should go to sleep](https://www.healthline.com/health/sleep/sleep-calculator)
as a starting point. It also includes a look-up table for when you should go to
bed depending on your wake-up time and the amount of sleep cycles you would like
to complete.

Also, the energy levels during the day are never the same. For me, the morning
hours are perfect. I have a high energy level and can easily focus on difficult
tasks. In the afternoon hours I am not so efficient and focused, while in the
late evening I am quite good from time to time (not always). This might be
different for you. I recommend that you observe yourself and try to put
difficult tasks where your energy levels are high and you have an easy time to
focus.

If I am talking of filters, I mean all the tactics helping me to focus on the
important things at the moment, thus filtering everything that wastes my and the
energy of the people I interact with.

In the next chapter and articles I will share some of those filters I am using
and the ideas behind each of them.

# My best filter

This is probably the best filter I have, but I have to admit that I am really
awful at applying it.

Just say **'No'** if you have too much on your plate!

You can read this in almost any post about task management and at least so many
books on the topic. I have also read it *a zillion* times, but only lately I
started applying it. Right now, I am thinking about it in two ways.

First, it is again about managing my energy. If I commit to this new task, will
I be able to do it with the amount of energy I have available and will the
overall energy that is required be enough to get it done in time? How much will
it slow down all the other tasks I have on my plate?

The second way I am thinking about it is about commitment and quality. If I now
commit, and I do not have sufficient energy, I will either rush the task and not
get it done entirely, or things will slip through. It will most likely not
finished in the way that matches with my personal standards. Will I be satisfied
with this in the end? Maybe it will be better to delegate and let somebody else
do the heavy lifting?

Compared to using estimates measured in hours, thinking about my tasks in terms
of required energy works better for me. Why this is the case, I have no idea
yet. Maybe it is similar to "measuring a user story in terms of complexity" as
opposed to "measuring a user story in terms of time".

For me it is really hard applying my best filter, especially if the upcoming
task is an technically exciting topic.

# Input-channel filter

I am applying the input-channel filter for many years now. What I am basically
doing is to disable all kind of distractions (notifiers) on my mobile and
computers.

* Regarding social media, e.g. WhatsApp, Facebook, Twitter, LinkedIn, I have
  disabled all notification popups for the mobile and web apps.
* Inside the company we are using MS Teams for communication. I have also
  deactivated all kind of notifications there, except e-mail. More details on
  this later.
* Regarding e-mail (MS Outlook), I have deactivated all desktop notifications
  and changed the default view on startup of MS Outlook to show my calendar and
  not the e-mail inbox. The latter I do to not get tempted to react on an
  e-mail, just because I have to check my schedule and stumble on an interesting
  headline by accident.

If I am working on a task or being in a meeting and I get constantly distracted,
I am not using the energy that I have in the most efficient way. Because the
second I react to that ping of MS Teams or the e-mail notification, I have
already lost my focus. Even if I would just see it popping up and do nothing.
Then, I need to spend an amount of energy again to get back into the topic. This
is getting even harder, if I would really react to the chat notification or
e-mail by actually answering it.

There is a whole book about this topic: [Deep Work by Cal Newport](https://www.calnewport.com/books/deep-work/).
If you need something for your reading list, I recommend adding it.

You might now think that you need to be available all of time. Maybe you even
think that what I am applying will never be possible for you. I see this a
little differently.

You should be available for your peers, but you do not have to be responsive all
of the time. Being available means, that you are reliable in answering requests
from the people you are working with. It does not mean, that you need to react
immediately all of the time.

In addition to this way of thinking, I am applying the following measures.

* I plan when I am going to check Social Media, MS Teams and MS Outlook. Usually
  I will do this in the afternoon as my energy levels are high in the morning
  and I want to use this as best as I can for the tasks I need to focus.
  Checking communication media is a low-energy task for me.
* I will check e-mail once per day, as e-mail is an asynchronous communication 
  media where it is sufficient to answer in 24 hours.
* I will check MS Teams ~2 times a day. This increased since I am working
  completely remotely, formerly I did it only once per day. Again, chat is
  asynchronous and it will be sufficient if I respond to it not immediately.
* In case there is something urgent, people know my mobile number which I of
  course will answer immediately.

Up to now, I have not told anybody about this system and up to now nobody did
complain that I am not available enough. Also the calls on my mobile are not
that often. Hopefully this will not change now as this is the first time I am
making my system transparent. As I already stated, the important thing is, that
the people your are interacting with get the feeling that you are available and
take care about there requests.

With this filter in place I feel much more focused and have the energy that is
available to me spend in an efficient way.

# Outlook

So, that's it for part 1 of the series. I hope you enjoyed it.

I have told you how I think about available energy and what a filter is. Then we
had a look at two of the tactics that I apply on a daily basis.

In the upcoming parts of this series we will look into more filters and tactics.
