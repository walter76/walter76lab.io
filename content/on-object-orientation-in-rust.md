+++
draft = true

title = "On Object-Orientation in Rust"
date = 2022-09-22

[taxonomies]
tags = ["rust", "oop"]
+++

Recently I had a discussion with a colleague on the advantages of the rust
programming language. There had been one remark from him, which made me start
thinking.

> I know teams that use rust because it is object-oriented as compared to the
> go language.

In this post I do not want to discuss whether rust is better than go or how you
can do object-oriented programming in go. What happened when he made this remark
was, that I knew that you can do object-oriented programming rust. But whenever
I tried to apply my OOP mindset (coming from languages like C/C++, Java and C#),
I ended up with weird designs and almost always had to fight the borrow checker.

In this article, I am doing an attempt in further understanding, what is the
difference between OOP in rust and the other OOP.

<!-- more -->
