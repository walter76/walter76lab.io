+++
template = "about.html"
+++

Hello and Welcome to my Website!

In my professional life, I am a Senior Software Architect for a big healthcare
provider. I love creating things and above all I like computers and developing
software.

At my job, I try to do as much coding as possible, but the responsibilities I
have been given do not allow me to create source code as much as I would love
to. Also the type of software you create and the technologies you can choose
from are limited in a big company. Therefore I spend my evenings and nights
fiddling with all kinds of technology.

Don't get me wrong. I love my job and right now, I could not imagine doing
anything else. But there is a second-self that is still the little curious kid
eager to learn how this brand new technology is working.

I have been experimenting with ESP32 and Raspberry PIs, developing web
applications with Google Firebase, fiddling with containers, my Manjaro Linux
operating system etc. I explore different programming languages: Java, C#, C++,
Python etc. Lately I got deeply into Rust.

There are two goals for this site:

* Sharing what I learned with others.
* Using it as a knowledge book for all the things in- and outside of my job.

I am pretty sure, that I will fail during my learning adventures and therefore I
might put things on this site that are not correct or free of mistakes. Please
drop me a message and point them out. Because only if I am aware of the errors I
can correct them. And, if you have a different opinion on a topic and want to
discuss, you are welcome to get in touch with me.
