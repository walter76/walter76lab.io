+++
title = "React: Why is useEffect called all of the time?"
date = 2020-05-17

[taxonomies]
tags = ["react", "javascript"]
+++

A while ago I was coding a Single-Page-Application with
[React](https://reactjs.org/) and hosted on Google's
[Firebase](https://firebase.google.com/). I have had some data stored in the 
Firestore database and was happily coding away with the React App running in
hot-reload development mode. All of a sudden I was not able to do any actions
with the database anymore. I got an exception in the JavaScript console 
originating from the Firebase API claiming that I had exceeded my daily read
quota.
<!-- more -->
At first I did not understand how this could happen as I have not experienced
any loops nor was doing any heavy database read operations. It was late in the
evening and I got tired, so I stored this problem away for the next day, also
with the perspective that the read quota will be reset.

The next evening, I started the App and checked the usage statistics in the
Firebase console. I observed that even if my application was just showing a 
static site with information from the database, I had a lot of read operations.
I stopped the development mode and the operations on the database stopped. Some
investigations later, I figured, that my `useEffect` hook got called in an
infinite loop, thus triggering the reload of my data from the database.

In this article we will dig into the mechanics of the `useEffect` hook with the
help of a simplified example. We will reproduce and analyze the problem
described above, providing a fix in the end.

If you have no idea about what React Hooks are, I recommend that you first learn
about the topic. There are plenty of articles on Google. For starters I can
recommend the official [React Hooks](https://reactjs.org/docs/hooks-intro.html)
documentation.

Also, I would like to give cudos to the excellent article
[How the useEffect Hook Works](https://daveceddia.com/useeffect-hook-examples/)
from [Dave Ceddia](https://daveceddia.com/). It helped me a lot in understanding
the issue and I can also recommend reading it as additional material.

# Creating a simple React project and reproducing the issue

To scaffold the new project, we will use [create-react-app](https://github.com/facebook/create-react-app)
from facebook. Open a terminal, navigate to the folder where you usually have
your source code and type:

```sh
npx create-react-app use-effect-hook-issue
```

Your React app will be created in a new folder `use-effect-hook-issue`. The 
process will take a while as `create-react-app` is downloading all dependencies
and setting everything up. Afterwards your console will look like it is shown in
the following screenshot.

![](01_create-react-app-done.png)

Let us now do some cleanup work, so we can concentrate on the problem with the
`useEffect` hook. Start your favorite IDE and open the newly created project. In
Visual Studio Code your project folder will look similar like in my Visual
Studio Code IDE.

![](02_clean-up-remove-files.png)

First step is to remove the files `App.css`, `App.test.js`, `index.css`,
`logo.svg` and `setupTests.js` as we do not need them. Next, open index.js and
change it.

```js
import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
```

In the next step, open `App.js` and also adapt it.

```js
import React from 'react'

export default () => <div>My React App!</div>
```

Start your React app in the console with `yarn start` and check if it is
working. It should just show `My React App!` in the browser window without any
formatting.

![](03_react-app-running.png)

Leave the application running in development mode, so we can instantly see our
changes. Next we want to reproduce the issue.

The example will not introduce any access to a database as I think this will add
too much stuff distracting us from the core of the problem. Let us change the
functional component and introduce the `useEffect` hook. Open `App.js` and edit
it like so.

```js
import React, { useEffect, useState } from 'react'

export default () => {
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    console.log('use effect called')

    setCounter(counter + 1)
  })

  return <div>Counter is {counter}!</div>
}
```

Switch to the browser and open the developer tools showing the console.

![](04_reproducing-the-issue-01.png)

Looks like that the lambda passed to `useEffect` is called very often, but at
the same time we also get a warning in the JavaScript console telling us about
the root cause of the problem: The state of `counter` is changed inside the
lambda passed to `useEffect`, thus triggering a re-render. Hm... but the initial
problem was not so obvious.

In fact, I was registering a listener in the lambda and changing the state
inside the event function. We do not need database access to simulate it. Let's
use `setTimeout` and do something with the state if the event is triggered. Open
`App.js`and change it.

```js
import React, { useEffect, useState } from 'react'

export default () => {
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    console.log('use effect called')
    
    const timeoutID = setTimeout(() => setCounter(counter + 1), 2 * 1000)

    return () => clearTimeout(timeoutID)
  })

  return <div>Counter is {counter}!</div>
}
```

Now in the lambda the timeout is set to 2 seconds and after the time is
over, the state is changed. We also clear the timeout with `clearTimeout` when
the component is unmounted. This is similar to unregistering the listener, which
I also had in my original source code.

Switch to the browser and check the JavaScript console in the developer tools.

![](05_reproducing-the-issue-02.png)

Here we go. No warning, but still the lambda gets called over and over again. We
have successfully reproduced the issue. Let's now try to understand what is
causing this.

# How `useEffect` is working

To understand React component lifecycle and the `useEffect` hook, it helps to
recap two important activities in the React framework.

* A component is **mounted,** when it is rendered for the first time to the DOM.
* A component is **unmounted,** when it is removed from the DOM, e.g. on hiding.

If you take a look into the official React documentation for `useEffect`, you
will find this statement:

> If you're familiar with React class lifecycle methods, you can think of
> `useEffect` Hook as `componentDidMount`, `componentDidUpdate`, and
> `componentWillUnmount` combined.

My personal opinion is, that this is totally confusing as it implies that you
can make a direct relation between the React class lifecycle methods and the
`useEffect` hook. I think, that you can make a connection there, but only after
you have understood how it works. So, please wipe the quote from the official
documentation from your mind and let's start fresh.

If you take a look at the example from the previous chapter, you see that we
have passed a function (lambda) to the `useEffect` hook. This lambda gets called
after every render of the component. A render usually takes place when the
component is shown for the first time (mounted) or after the state has been
updated. This means, with `useEffect` we get the option to run a *side effect*
after every render. In our case we execute the following lambda.

```js
console.log('use effect called')

const timeoutID = setTimeout(() => setCounter(counter + 1), 2 * 1000)

return () => clearTimeout(timeoutID)
```

Now notice that we are returning another function
`() => clearTimeout(timeoutID)`, let's call it a cleanup-lambda.

Returning a cleanup-lambda from the function passed to `useEffect` is optional.
If you do it, your cleanup-lambda will be called before the lambda passed to
`useEffect` will be called again. This happens after each render and before
unmounting the component. I have tried to describe the behavior in this sequence
diagram.

![](06_use-effect-sequence-diagram.png)

I do not have any knowledge of the deeper internals from React, therefore please
do not take the diagram literally. There are surely more components involved and
I have simplified all parts from React as simply a "React"-Object. Nevertheless,
it illustrates the main lifecycle events and which lambda gets called when.

The diagram shows the three typical events: mounting, update and unmounting. For
each of them the call order of the lambdas is shown. I have used colors to
improve the readability of the diagram.

To sum it up, I would like to take two quotes from
[How the useEffect Hook Works](https://daveceddia.com/useeffect-hook-examples/):

> `useEffect` runs after every render (by default), and can optionally clean up
> for itself before it runs again.

> Rather than thinking of `useEffect` as one function doing the job of 3
> separate lifecycles, it might be more helpful to think of it simply as a way
> to run side effects after render - including the potential cleanup you'd want
> to do before each on, and before unmounting.

# Back to the initial problem and how to fix it

So, finally the issue is fully understood. The lambda gets called for the first
time when the component is mounted, but actually it is only registering a event
hook (listener) and not doing anything. When the event is fired, our state gets
changed by the listener and therefore a re-render is triggered by the update.
Thus, causing an infinite render loop which is not detected by the React 
framework because of the delayed call when the event is triggered.

Luckily, the React framework has a solution to our problem. It is possible to
control, when the lambda which is provided in the `useEffect` hook gets called
by providing a second argument to the call of `useEffect`.

```js
const [value, setValue] = useState('initial')

useEffect(() => { 
  // This effect uses the `value` variable,
  // so it "depends on" `value`.
  console.log(value)
 }, [value]) // pass `value` as a dependency
```

The second argument provides an array of state variables upon which the
`useEffect` hook depends. In our example it is `value`. This means, the lambda
provided will only be run if `value` got changed and on initial render. Let's
adapt our code and check if this changes anything.

```js
useEffect(() => {
  console.log('use effect called')
  
  const timeoutID = setTimeout(() => setCounter(counter + 1), 2 * 1000)

  return () => clearTimeout(timeoutID)
}, [counter])
```

Not quite there yet. We have modelled the dependency, but still the hook gets
called on every trigger from the event. What we need is quite the opposite. We
want the lambda to be called only once: when the component is mounted.

There is a solution provided by React. If we pass an empty array, the lambda
will be only called once, when the component is mounted and the cleanup-lambda
will be called before the next update or unmounting of the component.

```js
useEffect(() => {
  console.log('use effect called')
  
  const timeoutID = setTimeout(() => setCounter(counter + 1), 2 * 1000)

  return () => clearTimeout(timeoutID)
}, [])
```

Now the lambda and the clean-up only gets called once.

It's possible that you now get a different message in the developer console:

```
Line 12:6:  React Hook useEffect has a missing dependency: 'counter'. Either
include it or remove the dependency array. You can also do a functional update
'setCounter(c => ...)' if you only need 'counter' in the 'setCounter' call 
react-hooks/exhaustive-deps
```

The static code analysis informs you about a potential error, but in our case
this happens by intention. We can fix it, by disabling the warning in the source
code.

```js
useEffect(() => {
  console.log('use effect called')
  
  const timeoutID = setTimeout(() => setCounter(counter + 1), 2 * 1000)

  return () => clearTimeout(timeoutID)
// eslint-disable-next-line react-hooks/exhaustive-deps
}, [])
```

# Summary

By reproducing our real-world problem with a very simplified example, we got the
opportunity to analyze the issue and get an understanding of the internals of
the `useEffect` hook. We then fixed the problem with means that are already
provided by the React framework.

If you want to dive deeper into the hook mechanics, I can again recommend the
excellent article
[How the useEffect Hook Works](https://daveceddia.com/useeffect-hook-examples/)
from [Dave Ceddia](https://daveceddia.com/) and the official documentation
[Using the Effect Hook](https://reactjs.org/docs/hooks-effect.html).

Hope you have had fun today!
