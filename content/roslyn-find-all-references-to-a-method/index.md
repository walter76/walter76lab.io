+++
title = "Roslyn: Find all references to a method"
date = 2020-07-09

[taxonomies]
tags = ["csharp", "roslyn"]
+++

This is my second article about [Roslyn](https://github.com/dotnet/roslyn). Ever
wondered, how Visual Studio Code finds out all the references of a method that
you click on?

![](02_vscode_find_all_references.png)

At least I did and with Roslyn I thought, it should be easy to get this done. In
this article we will create some toy domain API and find the usages of a method
to print them to the console.
<!-- more -->
If you are interested in a way to find all classes in a C# project, you can read
my previous article [Roslyn: Find all classes in a C# project](@/roslyn-find-all-classes-in-a-csharp-project/index.md).

# Create a .NET Core project

As usual, let's get started by creating a .NET Core project for toying around.
Open a command line, navigate to some directory where you would like to put your
source code and enter:

```powershell
mkdir find-usages
cd find-usages
dotnet new console
```

This will scaffold a new console project in the folder `find-usages`. After the
commands have finished, you should be able to run it with:

```powershell
dotnet run
```

The default implementation is to print `Hello World!` on the console. If you see
it, you are good to go.

# Implementing some demo code

We will need some demo code to analyze. The domain logic will be a very simple
contact list where you can just add some people and list them later on. Let's
start by adding a new file `Contact.cs` to our project.

```cs
namespace find_usages
{
    class Contact
    {
        public string Firstname { get; set; }
        public string Surname { get; set; }
    }
}
```

This is just a [POCO](https://en.wikipedia.org/wiki/Plain_old_CLR_object) to
represent a single contact. Next we need a class to store all of our contacts.
Let's add it as a new class in `Contacts.cs`.

```cs
using System.Collections.Generic;

namespace find_usages
{
    class Contacts
    {
        private readonly List<Contact> _contacts = new List<Contact>();

        public IReadOnlyCollection<Contact> Items { get => _contacts.AsReadOnly(); }

        public void Add(string firstname, string surname)
        {
            _contacts.Add(new Contact { Firstname = firstname, Surname = surname });
        }
    }
}
```

Again, very simple. Just a method to add a new contact and get access to all the
stored items. Finally we need some code to call what we have just created.
Change `Program.cs` as follows.

```cs
using System;

namespace find_usages
{
    class Program
    {
        static void Main(string[] args)
        {
            var romansContacts = new Contacts();
            romansContacts.Add("Remi", "Briggs");
            romansContacts.Add("Kurt", "Weller");
            romansContacts.Add("Edgar", "Reade");
            romansContacts.Add("Tasha", "Zapata");
            romansContacts.Add("William", "Patterson");

            Console.WriteLine("Romans Contacts");
            foreach (var contact in romansContacts.Items)
            {
                Console.WriteLine($"{contact.Surname}, {contact.Firstname}");
            }
        }
    }
}
```

Looks like we have found the contact list of Roman Briggs from the incredible
series [Blindspot](https://en.wikipedia.org/wiki/Blindspot_(TV_series)). If you
don't know it, you should check it out. :-)

Ok, let's run the code. Your output should look similar to this.

![](01_running_the_demo_code.png)

Before we can get started, let's do a quick refactoring as we do not plan to
touch the domain logic anymore and it would be nicer to have it separate. That
way we can fully focus on the Roslyn part of this article.

```cs
using System;

namespace find_usages
{
    class Program
    {
        static void ExecuteDomainLogic()
        {
            var romansContacts = new Contacts();
            romansContacts.Add("Remi", "Briggs");
            romansContacts.Add("Kurt", "Weller");
            romansContacts.Add("Edgar", "Reade");
            romansContacts.Add("Tasha", "Zapata");
            romansContacts.Add("William", "Patterson");

            Console.WriteLine("Romans Contacts");
            foreach (var contact in romansContacts.Items)
            {
                Console.WriteLine($"{contact.Surname}, {contact.Firstname}");
            }
        }

        static void Main(string[] args)
        {
            ExecuteDomainLogic();

            Console.WriteLine();
            Console.WriteLine("-----");
            Console.WriteLine();
         
         	// insert Roslyn code here
        }
    }
}
```

# Back to Roslyn and finding usages

Let's get back to Roslyn and our initial goal to find all the usages of some
method. Looking at our code it might be interesting to find out how and where we
have called the `Add`-method of `Contacts`.

As a first step we will need to add some packages to our project to be able to
load a project file and analyze the codebase. Open up a console, navigate to the
project root folder and type:

```sh
dotnet add package Microsoft.CodeAnalysis
dotnet add package Microsoft.Net.Compilers
dotnet add package Microsoft.Build.Locator
dotnet add package Microsoft.CodeAnalysis.CSharp.Workspaces
dotnet add package Microsoft.CodeAnalysis.Workspaces.MSBuild
```

This will add references to `Microsoft.CodeAnalysis`, `Microsoft.Net.Compilers`, `Microsoft.Build.Locator`, 
`Microsoft.CodeAnalysis.CSharp.Workspaces` and `Microsoft.CodeAnalysis.Workspaces.MSBuild`.
Your `find-usages.csproj` should now look like this.

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp3.1</TargetFramework>
    <RootNamespace>find_usages</RootNamespace>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="Microsoft.Build.Locator" Version="1.2.6" />
    <PackageReference Include="Microsoft.CodeAnalysis" Version="3.6.0" />
    <PackageReference Include="Microsoft.CodeAnalysis.CSharp.Workspaces" Version="3.6.0" />
    <PackageReference Include="Microsoft.CodeAnalysis.Workspaces.MSBuild" Version="3.6.0" />
    <PackageReference Include="Microsoft.Net.Compilers" Version="3.6.0">
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
      <PrivateAssets>all</PrivateAssets>
    </PackageReference>
  </ItemGroup>

</Project>
```

Back to coding. As a second step we will create a MS Build workspace and load
the project file provided as the first argument to the program. Add the
following code to `Program.cs`.

```cs
// <-- snip -->
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.MSBuild;
// <-- snap -->

// <-- snip -->
// insert Roslyn code here
var projectFilePath = args[0];

MSBuildLocator.RegisterDefaults();
var workspace = MSBuildWorkspace.Create();
var project = workspace.OpenProjectAsync(projectFilePath).Result;
// <-- snap -->
```

Now we want to parse each source file in the project and get a list of all the
method names. Add some more `using` statements to our `Program.cs` such that it
looks like this.

```cs
// <-- snip -->
using System;
using System.Linq;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.MSBuild;
// <-- snap -->
```

Next, add the following code at the bottom of the `Main`-method:

```cs
// <-- snip -->
foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
{
    var syntaxTree = document.GetSyntaxTreeAsync().Result;
    var syntaxTreeRoot = syntaxTree.GetRoot();
    var methodDeclarations =
        syntaxTreeRoot
        .DescendantNodes()
        .Where(n => n is MethodDeclarationSyntax)
        .Cast<MethodDeclarationSyntax>();

    foreach (var methodDeclaration in methodDeclarations)
    {
        Console.WriteLine(methodDeclaration.Identifier.Value);
    }
}
// <-- snap -->
```

I think this needs a little bit more explanation. Let me walk you through it.
With the `foreach`-statement we parse all the documents that have a valid
[Syntax Tree](https://github.com/dotnet/roslyn/wiki/Roslyn-Overview#syntax-trees).
For each of those documents, we get all the descendant nodes and filter those
that are not of type `MethodDeclarationSyntax`. For convenience, we cast the
result with `Cast<MethodDeclarationSyntax>()`. That way we get a nicely typed
enumeration. Then, we iterate over all the found method declarations and print
the identifier to the console.

If you run the program with `dotnet run .\\find-usages.csproj` you should get
the following output.

![](03_all_method_identifiers_to_console.png)

# A little bit of refactoring

Our code got a little bit complex. Let's do some refactoring so we can more
easily add the next incremental step for our project.

Create a new file `UsageFinder.cs` add a class `UsageFinder` and move the Roslyn
code from `Program.cs` there. The file `UsageFinder.cs`should now look like.

```cs
using System;
using System.Linq;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.MSBuild;

namespace find_usages
{
    class UsageFinder
    {
        public void FindAllUsages(string projectFilePath)
        {
            MSBuildLocator.RegisterDefaults();
            var workspace = MSBuildWorkspace.Create();
            var project = workspace.OpenProjectAsync(projectFilePath).Result;

            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                var syntaxTree = document.GetSyntaxTreeAsync().Result;
                var syntaxTreeRoot = syntaxTree.GetRoot();
                var methodDeclarations =
                    syntaxTreeRoot
                    .DescendantNodes()
                    .Where(n => n is MethodDeclarationSyntax)
                    .Cast<MethodDeclarationSyntax>();

                foreach (var methodDeclaration in methodDeclarations)
                {
                    Console.WriteLine(methodDeclaration.Identifier.Value);
                }
            }
        }
    }
}
```

Next, change `Program.cs` to call the newly created method like so.

```cs
// <-- snip -->
// insert Roslyn code here
var usageFinder = new UsageFinder();
usageFinder.FindAllUsages(args[0]);
// <-- snap -->
```

Let us now do some more refactoring in `UsageFinder.cs` to make adding further
incremental features easier. First, extract opening the project into a private
method `OpenProject`.

```cs
// <-- snip -->
private Project OpenProject(string projectFilePath)
{
    MSBuildLocator.RegisterDefaults();
    var workspace = MSBuildWorkspace.Create();
    return workspace.OpenProjectAsync(projectFilePath).Result;
}
// <-- snap -->
```

Next, extract getting the method declarations from a document in its own
function `GetMethodDeclarations`.

```cs
// <-- snip -->
private IEnumerable<MethodDeclarationSyntax> GetMethodDeclarations(Document sourceFileDocument)
{
    var syntaxTree = sourceFileDocument.GetSyntaxTreeAsync().Result;
    var syntaxTreeRoot = syntaxTree.GetRoot();

    return syntaxTreeRoot
        .DescendantNodes()
        .Where(n => n is MethodDeclarationSyntax)
        .Cast<MethodDeclarationSyntax>();
}
// <-- snap -->
```

The last step is to carve-out the part which prints to the console. For that, we
extract getting all method declarations in its own method
`GetMethodDeclarationsFromProject`.

```cs
// <-- snip -->
private IEnumerable<MethodDeclarationSyntax> GetMethodDeclarationsFromProject(Project project)
{
    var allMethodDeclarations = new List<MethodDeclarationSyntax>();

    foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
    {
        allMethodDeclarations.AddRange(GetMethodDeclarations(document));
    }

    return allMethodDeclarations;
}
// <-- snap -->
```

`FindAllUsages` should now look like this.

```cs
// <-- snip -->
public void FindAllUsages(string projectFilePath)
{
    var project = OpenProject(projectFilePath);
    var allMethodDeclarations = GetMethodDeclarationsFromProject(project);

    foreach (var methodDeclaration in allMethodDeclarations)
    {
        Console.WriteLine(methodDeclaration.Identifier.Value);
    }
}
// <-- snap -->
```

The program should still do its job. If you run it, it should yield the
following output with a few more methods printed out because of our refactoring
steps.

![](04_first_refactoring.png)

# Deeper into Roslyn: the Semantic Model

We are almost done. This step will involve finding all the usages. For that, we
need to restructure the code a little bit more.

Parsing the Syntax-Tree will not be enough for finding all the references to a
method as it is only a static representation of your source code and has not
been compiled. We need the so-called [Semantic Model](https://github.com/dotnet/roslyn/wiki/Getting-Started-C%23-Semantic-Analysis)
for this. This can be created from the document which defines the method where
we would like to find the usages for. After you have obtained it, you can query
it for questions like "What names are in scope at this location?", "What members
are accessible from this method?" or "What references exist to this
method/identifier?".

Let's introduce a new class `Method` at the top of `UsageFinder.cs`.

```cs
// <-- snip -->
class Method
{
    public MethodDeclarationSyntax Declaration { get; set; }
    public Document Document { get; set; }
}
// <-- snap -->
```

We need this class to get access to the document for creating the Semantic Model
later on. Let's extend the functionality of `UsageFinder` such as it returns a
list of `Method` instead of `MethodDeclarationSyntax` only.

```cs
// <-- snip -->
public void FindAllUsages(string projectFilePath)
{
    var project = OpenProject(projectFilePath);
    var allMethods = GetMethodsFromProject(project);

    foreach (var method in allMethods)
    {
        Console.WriteLine(method.Declaration.Identifier.Value);
    }
}
// <-- snap -->

// <-- snip -->
private IEnumerable<Method> GetMethodsFromProject(Project project)
{
    var allMethods = new List<Method>();

    foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
    {
        allMethods.AddRange(GetMethods(document));
    }

    return allMethods;
}

private IEnumerable<Method> GetMethods(Document sourceFileDocument)
{
    var syntaxTree = sourceFileDocument.GetSyntaxTreeAsync().Result;
    var syntaxTreeRoot = syntaxTree.GetRoot();

    return syntaxTreeRoot
        .DescendantNodes()
        .Where(n => n is MethodDeclarationSyntax)
        .Cast<MethodDeclarationSyntax>()
        .Select(m => new Method { Declaration = m, Document = sourceFileDocument });
}
// <-- snap -->
```

Run the program again to check if it is still working as expected.

Currently we get a list of all the methods in the project. But we would like to
only check for usages of `Add`. Let's get the instance of `Method` which
encapsulates the `MethodDeclaraionSyntax` for this method.

```cs
// <-- snip -->
public void FindAllUsages(string projectFilePath)
{
    var methodIdentifier = "Add";
    var project = OpenProject(projectFilePath);
    var allMethods = GetMethodsFromProject(project);

    var addMethod = allMethods
        .SingleOrDefault(
            m => m.Declaration.Identifier.ValueText == methodIdentifier);

    if (addMethod == null)
    {
        Console.WriteLine($"Method declaration for {methodIdentifier} not found!");
        return;
    }

    Console.WriteLine(addMethod.Declaration.Identifier.ValueText);
}
// <-- snap -->
```

I am using [LINQ](https://docs.microsoft.com/de-de/dotnet/csharp/programming-guide/concepts/linq/)
to get the reference to the `Add` method declaration.

Now let's find the references. Add the following code at the end of
`FindAllUsages`.

```cs
// <-- snip -->
var semanticModel =
    addMethod.Document.GetSemanticModelAsync().Result;
var addMethodSymbol =
    semanticModel.GetDeclaredSymbol(addMethod.Declaration);

var referencesToAddMethod =
    SymbolFinder.FindReferencesAsync(
        addMethodSymbol, addMethod.Document.Project.Solution).Result;
            
foreach (var reference in referencesToAddMethod)
{
    foreach (var location in reference.Locations)
    {
        Console.Write("  > ");
        Console.WriteLine(
            location.Location.GetMappedLineSpan().ToString());
    }
}
// <-- snap -->
```

So, what is happening here. First, I construct the Semantic Model by calling the
method `GetSemanticModelAsync()` on the member `Document`. As this is now
compiled, we can query the model for the symbol of `Add`. Next, we use the
`SymbolFinder` to query the complete solution for all references to `Add`.
Finally, we iterate the result enumeration and print all the locations to the
console.

Run the program. You should get the following output.

![](05_references_to_methods.png)

You now see a list of references with the source file name and the span where
the method is called. If you go to your IDE and check it you should be able to
verify that it actually works.

![](06_references_in_vscode.png)

The line numbers are increased by one as Roslyn starts counting from 0. But
other than that it looks ok.

For you reference I'll add the whole `UsageFinder.cs` below in case something
went wrong while we were doing refactoring's and changing the code.

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.FindSymbols;
using Microsoft.CodeAnalysis.MSBuild;

namespace find_usages
{
    class Method
    {
        public MethodDeclarationSyntax Declaration { get; set; }
        public Document Document { get; set; }
    }

    class UsageFinder
    {
        public void FindAllUsages(string projectFilePath)
        {
            var methodIdentifier = "Add";
            var project = OpenProject(projectFilePath);
            var allMethods = GetMethodsFromProject(project);

            var addMethod = allMethods
                .SingleOrDefault(
                    m => m.Declaration.Identifier.ValueText == methodIdentifier);

            if (addMethod == null)
            {
                Console.WriteLine($"Method declaration for {methodIdentifier} not found!");
                return;
            }

            Console.WriteLine(addMethod.Declaration.Identifier.ValueText);

            var semanticModel =
                addMethod.Document.GetSemanticModelAsync().Result;
            var addMethodSymbol =
                semanticModel.GetDeclaredSymbol(addMethod.Declaration);

            var referencesToAddMethod =
                SymbolFinder.FindReferencesAsync(
                    addMethodSymbol, addMethod.Document.Project.Solution).Result;
            
            foreach (var reference in referencesToAddMethod)
            {
                foreach (var location in reference.Locations)
                {
                    Console.Write("  > ");
                    Console.WriteLine(location.Location.GetMappedLineSpan().ToString());
                }
            }
        }

        private Project OpenProject(string projectFilePath)
        {
            MSBuildLocator.RegisterDefaults();
            var workspace = MSBuildWorkspace.Create();
            return workspace.OpenProjectAsync(projectFilePath).Result;
        }

        private IEnumerable<Method> GetMethodsFromProject(Project project)
        {
            var allMethods = new List<Method>();

            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                allMethods.AddRange(GetMethods(document));
            }

            return allMethods;
        }

        private IEnumerable<Method> GetMethods(Document sourceFileDocument)
        {
            var syntaxTree = sourceFileDocument.GetSyntaxTreeAsync().Result;
            var syntaxTreeRoot = syntaxTree.GetRoot();

            return syntaxTreeRoot
                .DescendantNodes()
                .Where(n => n is MethodDeclarationSyntax)
                .Cast<MethodDeclarationSyntax>()
                .Select(m => new Method { Declaration = m, Document = sourceFileDocument });
        }
    }
}
```

# Summary

In this article we learned how to parse a complete project, find all method
declarations and then used the Semantic Model to query for all the references to
one method. I hope you enjoyed this article and learned more about Roslyn.
