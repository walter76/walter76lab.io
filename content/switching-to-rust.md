+++
title = "Switching to Rust!"
date = 2024-01-13

[taxonomies]
tags = ["rust"]
+++

**I tried out Rust, and I do not want to go back!**

<!-- more -->

Around 3 years back, in the middle of the pandemic, I started learning Rust. It has been a steep
learning curve. During my attempts to develop something, I sometimes had some doubts and even
switched back to .NET C# for a short period of time. In the end, I did not give up. And then, it
somehow made _click_. All of a sudden, things like complaints from the borrow checker started to make
sense. I experienced the beauty of Rust and today, I do not want to go back.

Taking part regularly in the [Advent Of Code](https://adventofcode.com/) surely helped a lot. The
great thing about AoC is, that it is all about the learning and the community. The puzzles are very
challenging and I never can solve all of them in the given timeframe. But every year, I ended up
being a better Rust developer.

There had been also other resources, but I think the key was, that I constantly practiced Rust
every day and made it a habit.

I will not write why Rust is a great programming language. More then enough blog posts have been
written about this topic.

.NET C# is still a programming language that I use in my day job. So, I might write about it once in
a while if I learn something interesting.

