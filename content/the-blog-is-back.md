+++
title = "The blog is back on"
date = 2022-03-02
+++

Wow! Almost one and a half year gone since I last wrote something for my blog.
Besides me either not being in the mood of writing or having lack of motivation,
a lot of things happened in the meanwhile.
<!-- more -->
I had three attempts on making a revamp of the blog site.

The first one was giving it some different look. It went live, but I have not
been really lucky with it.

The second one, I have been trying a slightly different frontend stack by getting
rid of [mui](https://mui.com/) and using [styled components](https://styled-components.com/)
instead. Although, I like this new approach better because it is less complicated,
it never got finished and went live.

Now, you are looking at the third attempt. I now discarded the idea of using my
self-made web application. Instead, I am using the static site generator [Zola](https://www.getzola.org/)
with the [after-dark](https://github.com/getzola/after-dark) theme. That way, I
can fully concentrate on writing and don't have to deal with the implementation
details of the blog itself. So far, I like it, but I also have some ideas on how
to improve the tool-chain and blog itself.

I have also changed the name to **my coding hut**. I never really was happy with
the previous name and with the invasion of the Ukraine by the Russian government,
I do not consider a term "refuge" to be a good choice anymore.

Then, I went deeper into the [Rust](https://www.rust-lang.org/) programming
language as ever before. The learning curve was very steep, and I am by far not
an expert yet. But I feel at home now and Rust will be my favorite language for
the foreseeable future. There is still a lot to learn and explore. So, you
should expect some posts on Rust in the future.

I plan to write more frequently now. First, I have to finish the series on the
Corona restaurant guest registration app: [Part 1 - Creating a Corona restaurant guest registration app](@/creating-a-corona-restaurant-tracking-app-part1/index.md).
The topic itself is kind of outdated now, but for learning purposes it is still
valid. I will most likely migrate to [styled components](https://styled-components.com/)
on the frontend.

So much for now!
